package com.candaceyoung.app.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.candaceyoung.app.R;
import com.candaceyoung.app.RetrofitApi.ApiClient;
import com.candaceyoung.app.interfaces.ApiInterface;
import com.candaceyoung.app.model.SendPushModel;
import com.candaceyoung.app.utils.CandaceyoungPreferences;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendPushActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SendPushActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SendPushActivity.this;

    /*
     * Widgets
     * */

    @BindView(R.id.txtHeadingTV)
    TextView txtHeadingTV;

    @BindView(R.id.imgEditIV)
    ImageView imgEditIV;

    @BindView(R.id.imgMenuIV)
    ImageView imgMenuIV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.editMessageET)
    EditText editMessageET;
    @BindView(R.id.txtSendTV)
    TextView txtSendTV;
    @BindView(R.id.editTitleET)
    EditText editTitleET;
    @BindView(R.id.titleRL)
    RelativeLayout titleRL;
    @BindView(R.id.messageRL)
    RelativeLayout messageRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_push);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        txtHeadingTV.setText(R.string.send_push);
        imgEditIV.setVisibility(View.GONE);
        imgMenuIV.setVisibility(View.GONE);
        imgBackIV.setVisibility(View.VISIBLE);
        editTextSelector(editTitleET, titleRL, "");
        editTextSelector(editMessageET, messageRL, "");
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", CandaceyoungPreferences.readString(mActivity,CandaceyoungPreferences.ID,null));
        mMap.put("title", editTitleET.getText().toString().trim());
        mMap.put("description", editMessageET.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeSendPushApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.sendPushRequest(mParam()).enqueue(new Callback<SendPushModel>() {
            @Override
            public void onResponse(Call<SendPushModel> call, Response<SendPushModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SendPushModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    Toast.makeText(mActivity, mModel.getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SendPushModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());


            }
        });
    }

    @OnClick({R.id.txtSendTV, R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtSendTV:
                performPushClick();
                break;
            case R.id.imgBackIV:
                performBackClick();
                break;
        }
    }

    private void performBackClick() {
        onBackPressed();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void performPushClick() {
        if (isValidate()) {
        if (!isNetworkAvailable(mActivity)) {
            showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeSendPushApi();
        }
    }}

    /*
     * Set up validations
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editTitleET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.enter_title));
            flag = false;
        } else if ((editMessageET.getText().toString().trim()).equals("")) {
            showAlertDialog(mActivity, getString(R.string.enter_message));
            flag = false;
        }else if ((editMessageET.getText().toString().trim()).length()>100) {
            showAlertDialog(mActivity, getString(R.string.enter_valid_desc));
            flag = false;
        }
        return flag;
    }

}

