package com.candaceyoung.app.activities;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.candaceyoung.app.R;
import com.candaceyoung.app.RetrofitApi.ApiClient;
import com.candaceyoung.app.interfaces.ApiInterface;
import com.candaceyoung.app.model.SignUpModel;
import com.candaceyoung.app.utils.CandaceyoungPreferences;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SignUpActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SignUpActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.editFirstNameET)
    EditText editFirstNameET;
    @BindView(R.id.editLastNameET)
    EditText editLastNameET;
    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
    @BindView(R.id.txtSignUpTV)
    TextView txtSignUpTV;
    @BindView(R.id.txtAlreadyHaveAccountTV)
    TextView txtAlreadyHaveAccountTV;
    String strDeviceToken;
    @BindView(R.id.emailRL)
    RelativeLayout emailRL;
    @BindView(R.id.passRL)
    RelativeLayout passRL;
    @BindView(R.id.FirstNameRL)
    RelativeLayout FirstNameRL;
    @BindView(R.id.LastNameRL)
    RelativeLayout LastNameRL;
    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        FirebaseApp.initializeApp(this);
        getDeviceToken();
        editTextSignupSelector(editEmailET, emailRL, "");
        editTextSignupSelector(editPasswordET, passRL, "");
        editTextSignupSelector(editFirstNameET, FirstNameRL, "");
        editTextSignupSelector(editLastNameET, LastNameRL, "");
    }

    private void getDeviceToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        strDeviceToken = task.getResult();

                        CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.DEVICE_TOKEN, strDeviceToken);
                        // Log and toast
                        Log.d(TAG, "Token********   " + strDeviceToken);
                    }
                });
    }

    @OnClick({R.id.txtSignUpTV, R.id.txtAlreadyHaveAccountTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtSignUpTV:
                performSignUpClick();
                break;
            case R.id.txtAlreadyHaveAccountTV:
                    performSignInClick();
                break;
        }
    }

    private void performSignInClick() {
        Intent intent = new Intent(mActivity, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private void performSignUpClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeSignUpApi();
            }
        }
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("first_name", editFirstNameET.getText().toString().trim());
        mMap.put("last_name", editLastNameET.getText().toString().trim());
        mMap.put("email", editEmailET.getText().toString().trim());
        mMap.put("password", editPasswordET.getText().toString().trim());
        mMap.put("device_type", "1");
        mMap.put("device_token", strDeviceToken);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void executeSignUpApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.signUpRequest(mParam()).enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SignUpModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    showToast(mActivity, mModel.getMessage());
//                    CandaceyoungPreferences.writeBoolean(mActivity, CandaceyoungPreferences.ISLOGIN, true);
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.ID, mModel.getData().getUserId());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.EMAIL, mModel.getData().getEmail());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.FIRST_NAME, mModel.getData().getFirstName());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.LAST_NAME, mModel.getData().getLastName());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.ROLE, mModel.getData().getRole());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.PASSWORD, mModel.getData().getPassword());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.FLAG_PHOTO, mModel.getData().getFlagPhoto());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.COUNTRY, mModel.getData().getCountry());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.FLAG_PHOTO, mModel.getData().getFlagPhoto());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.VERIFIED, mModel.getData().getVerified());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.IMAGE, response.body().getData().getPhoto());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.LONGITUDE, mModel.getData().getLongitude());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.COUNTRY_CODE, mModel.getData().getCountryCode());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.DEVICE_TYPE, mModel.getData().getDeviceType());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.DEVICE_TOKEN, mModel.getData().getDeviceToken());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.DISABLED, mModel.getData().getDisabled());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.DESCRIPTION, response.body().getData().getDescription());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.CREATION_AT, response.body().getData().getCreatedAt());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.ALLOW_PUSH,mModel.getData().getAllowPush());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.DEVICE_TYPE,mModel.getData().getAllowPush());
//                    CandaceyoungPreferences.writeString(mActivity, CandaceyoungPreferences.STATUS,mModel.getData().getStatus());
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
                dismissProgressDialog();
                showToast(mActivity,"failure");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Set up validations for Sign In fields
     * */
    /*
     * Check Validations of views
     * */
    public boolean isValidate() {
        boolean flag = true;
        if (editFirstNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.enter_first_name));
            flag = false;
        } else if (editLastNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_last_name));
            flag = false;
        } else if (editEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().length() < 6) {
            showAlertDialog(mActivity, getString(R.string.please_enter_min_6_digit_pass));
            flag = false;
        }
        return flag;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }
}
