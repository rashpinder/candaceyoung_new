package com.candaceyoung.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.candaceyoung.app.R;
import com.candaceyoung.app.RetrofitApi.ApiClient;
import com.candaceyoung.app.adapters.LinksAdapter;
import com.candaceyoung.app.fragments.AdminFragment;
import com.candaceyoung.app.fragments.AnnouncementFragment;
import com.candaceyoung.app.fragments.ChoirFragment;
import com.candaceyoung.app.fragments.HomeFragment;
import com.candaceyoung.app.fragments.MensMinistryFragment;
import com.candaceyoung.app.fragments.ProfileFragment;
import com.candaceyoung.app.fragments.WomensMinistryFragment;
import com.candaceyoung.app.interfaces.ApiInterface;
import com.candaceyoung.app.model.HomeModel;
import com.candaceyoung.app.model.LinkModel;
import com.candaceyoung.app.model.LogoutModel;
import com.candaceyoung.app.utils.CandaceyoungPreferences;
import com.candaceyoung.app.utils.Constants;
import com.candaceyoung.app.utils.SimpleSideDrawer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LinksActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = LinksActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = LinksActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.linksRV)
    RecyclerView linksRV;
    LinksAdapter mLinksAdapter;
    List<HomeModel.CategoryDetail> mCategoryList;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;

    @BindView(R.id.txtHeadingTV)
    TextView txtHeadingTV;

    @BindView(R.id.imgEditIV)
    ImageView imgEditIV;

    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.imgAddIV)
    ImageView imgAddIV;
    @BindView(R.id.imgMenuIV)
    ImageView imgMenuIV;

    public SimpleSideDrawer mSimpleSideDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_links);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        txtHeadingTV.setText("Links");
        imgEditIV.setVisibility(View.GONE);
        imgBackIV.setVisibility(View.VISIBLE);
        imgMenuIV.setVisibility(View.GONE);
        getLinkList();
    }

    /*
             Execute api
              */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", CandaceyoungPreferences.readString(mActivity, CandaceyoungPreferences.ID, null));
        mMap.put("search", "");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void getLinkList() {
        if (!isNetworkAvailable(mActivity)) {
            showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetLinkDetailsApi();
        }
    }

    private void executeGetLinkDetailsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.homeListRequest(mParam()).enqueue(new Callback<HomeModel>() {
            @Override
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                HomeModel mModel = response.body();
                mCategoryList = new ArrayList<>();
                mCategoryList = mModel.getCategoryDetails();
                if (mModel.getStatus() == 1) {
                    setHomeAdapter();
                    txtNoDataFountTV.setVisibility(View.GONE);
                } else {
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                    txtNoDataFountTV.setText(mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<HomeModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    private void setHomeAdapter() {
        mLinksAdapter = new LinksAdapter(mActivity, mCategoryList);
        linksRV.setLayoutManager(new LinearLayoutManager(mActivity));
        linksRV.setAdapter(mLinksAdapter);
        mLinksAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @OnClick({R.id.imgAddIV, R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgAddIV:
                performAddClick();
                break;
            case R.id.imgBackIV:
                performBackClick();
                break;
        }
    }

    private void performAddClick() {
        startActivity(new Intent(mActivity, AddLinksActivity.class));
    }

    private void performBackClick() {
        onBackPressed();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

