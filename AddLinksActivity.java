package com.candaceyoung.app.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.candaceyoung.app.R;
import com.candaceyoung.app.RetrofitApi.ApiClient;
import com.candaceyoung.app.adapters.HomeAdapter;
import com.candaceyoung.app.fragments.AdminFragment;
import com.candaceyoung.app.fragments.AnnouncementFragment;
import com.candaceyoung.app.fragments.ChoirFragment;
import com.candaceyoung.app.fragments.HomeFragment;
import com.candaceyoung.app.fragments.MensMinistryFragment;
import com.candaceyoung.app.fragments.ProfileFragment;
import com.candaceyoung.app.fragments.WomensMinistryFragment;
import com.candaceyoung.app.interfaces.ApiInterface;
import com.candaceyoung.app.model.HomeModel;
import com.candaceyoung.app.model.LinkModel;
import com.candaceyoung.app.model.LogoutModel;
import com.candaceyoung.app.utils.CandaceyoungPreferences;
import com.candaceyoung.app.utils.Constants;
import com.candaceyoung.app.utils.SimpleSideDrawer;
import com.joooonho.SelectableRoundedImageView;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddLinksActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = AddLinksActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = AddLinksActivity.this;

    /*
     * Widgets
     * */

    @BindView(R.id.txtHeadingTV)
    TextView txtHeadingTV;

    @BindView(R.id.imgEditIV)
    ImageView imgEditIV;

    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;

    @BindView(R.id.imgMenuIV)
    ImageView imgMenuIV;

    @BindView(R.id.imgAddLinkIV)
    SelectableRoundedImageView imgAddLinkIV;
    @BindView(R.id.titleRL)
    RelativeLayout titleRL;
    @BindView(R.id.linkRL)
    RelativeLayout linkRL;
    @BindView(R.id.typeSpinner)
    Spinner typeSpinner;
    @BindView(R.id.editTitleET)
    EditText editTitleET;
    @BindView(R.id.editLinkET)
    EditText editLinkET;
    @BindView(R.id.editDescET)
    EditText editDescET;

    @BindView(R.id.descRL)
    RelativeLayout descRL;
    @BindView(R.id.txtSubmitTV)
    TextView txtSubmitTV;
    String category;
    String cat_id;
    String sel_category;
    String sel_cat_id;

    List<HomeModel.CategoryDetail> mCategoryList;
    List<String> mCategoriesList;
    List<String> mCategoryIdList;

    public SimpleSideDrawer mSimpleSideDrawer;
    Bitmap selectedImage;
    String mBase64Image = "";
    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_links);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        txtHeadingTV.setText("Add Link");
        imgEditIV.setVisibility(View.GONE);
        imgBackIV.setVisibility(View.VISIBLE);
        imgMenuIV.setVisibility(View.GONE);
        editTextSelector(editTitleET, titleRL, "");
        editTextSelector(editLinkET, linkRL, "");
        editTextSelector(editDescET, descRL, "");
        getLinkList();

    }

    /*
                 Execute api
                  */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", CandaceyoungPreferences.readString(mActivity, CandaceyoungPreferences.ID, null));
        mMap.put("search", "");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void getLinkList() {
        if (!isNetworkAvailable(mActivity)) {
            showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetLinkDetailsApi();
        }
    }

    private void executeGetLinkDetailsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.homeListRequest(mParam()).enqueue(new Callback<HomeModel>() {
            @Override
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                HomeModel mModel = response.body();
                mCategoryList = new ArrayList<>();
                mCategoriesList = new ArrayList<>();
                mCategoryIdList = new ArrayList<>();
                mCategoryList = mModel.getCategoryDetails();
                if (mModel.getStatus() == 1) {
                    for (int i = 0; i < mCategoryList.size(); i++) {
                        category = mCategoryList.get(i).getName();
                        cat_id = mCategoryList.get(i).getCId();
                        mCategoriesList.add(category);
                        mCategoryIdList.add(cat_id);
                        setTypeSpinner();
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<HomeModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private Map<String, String> mAddParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", CandaceyoungPreferences.readString(mActivity, CandaceyoungPreferences.ID, null));
        mMap.put("title", editTitleET.getText().toString().trim());
        mMap.put("selectType", sel_cat_id);
        mMap.put("link", editLinkET.getText().toString().trim());
        mMap.put("description", editDescET.getText().toString().trim());
        mMap.put("image", mBase64Image);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void performSubmitClick() {
        if (isValidate()) {
        if (!isNetworkAvailable(mActivity)) {
            showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeAddLinkDetailsApi();
        }
    }}

    private void executeAddLinkDetailsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addlinkRequest(mAddParam()).enqueue(new Callback<LinkModel>() {
            @Override
            public void onResponse(Call<LinkModel> call, Response<LinkModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                LinkModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, mModel.getMessage());
                    finish();
                } else {
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<LinkModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    @OnClick({R.id.imgAddLinkIV, R.id.imgBackIV, R.id.txtSubmitTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgAddLinkIV:
                performImageClick();
                break;
            case R.id.imgBackIV:
                performBackClick();
                break;
            case R.id.txtSubmitTV:
                performSubmitClick();
                break;

        }
    }


    /*
     * Gender adapter
     * */
    private void setTypeSpinner() {
        Typeface font = Typeface.createFromAsset(mActivity.getAssets(),
                "Roboto-Regular.ttf");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mCategoriesList) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_type_spinner, null);
                }
                TextView itemTextView = v.findViewById(R.id.itemTypeTV);
//                if (position == 0) {
//                    itemTextView.setVisibility(View.INVISIBLE);
//                }
                itemTextView.setText(mCategoriesList.get(position));
                itemTextView.setTypeface(font);
                return v;
            }
        };

        typeSpinner.setAdapter(adapter);

        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                sel_cat_id = mCategoryIdList.get(position);
                Log.e(TAG, "cat_id" + sel_cat_id);
                if (position == 0) {
                    ((TextView) view).setTextSize(14);
                    ((TextView) view).setTypeface(font);
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorDarkGrey));
                } else {
                    ((TextView) view).setTextSize(14);
                    ((TextView) view).setTypeface(font);
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorDarkGrey));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    /* Camera Gallery functionality
     * */
    private void performImageClick() {
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private void onSelectImageClick() {
        CropImage.startPickImageActivity(mActivity);
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setAspectRatio(70, 70)
                .setMultiTouchEnabled(false)
                .start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(mActivity, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(mActivity, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                try {

                    final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
                    selectedImage = BitmapFactory.decodeStream(imageStream);
                    String path = MediaStore.Images.Media.insertImage(mActivity.getContentResolver(), selectedImage, "IMG_", null);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 10, baos);
                    RequestOptions options = new RequestOptions()
                            .placeholder(R.drawable.ic_upload_image)
                            .error(R.drawable.ic_upload_image)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .skipMemoryCache(true)
                            .dontAnimate()
                            .dontTransform();

                    if (path != null) {
                        Glide.with(mActivity).load(path)
                                .apply(options)
                                .into(imgAddLinkIV);

                    }
                    mBase64Image = encodeTobase64(selectedImage);

                    Log.e(TAG, "**Image Base 64**" + mBase64Image);


                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(mActivity, "Cropping failed: " + result.getError());
            }
        }
    }

    private String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 30, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    private void performBackClick() {
        onBackPressed();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /*
     * Set up validations
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (mBase64Image.equals("")) {
            showAlertDialog(mActivity, getString(R.string.select_image));
            flag = false;
        } else if (editTitleET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.enter_title));
            flag = false;
        } else if (editLinkET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.enter_link));
            flag = false;
        } else if (!isValidUrl(editLinkET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.enter_valid_link));
            flag = false;
        } else if (!editLinkET.getText().toString().trim().contains("https://")) {
            showAlertDialog(mActivity, getString(R.string.enter_valid_link));
            flag = false;
        } else if ((editDescET.getText().toString().trim().equals(""))) {
            showAlertDialog(mActivity, getString(R.string.enter_desc));
            flag = false;
        } else if ((editDescET.getText().toString().trim().equals(""))) {
            showAlertDialog(mActivity, getString(R.string.enter_desc));
            flag = false;
        }else if ((editDescET.getText().toString().trim().length()>100)) {
            showAlertDialog(mActivity, getString(R.string.enter_valid_desc));
            flag = false;
        }
        return flag;
    }

    private boolean isValidUrl(String url) {
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(url.toLowerCase());
        return m.matches();
    }
}



