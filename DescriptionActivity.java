package com.candaceyoung.app.activities;

import android.app.Activity;
import android.media.Image;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.candaceyoung.app.R;
import com.candaceyoung.app.RetrofitApi.ApiClient;
import com.candaceyoung.app.adapters.DescriptionAdapter;
import com.candaceyoung.app.interfaces.ApiInterface;
import com.candaceyoung.app.model.DescriptionModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DescriptionActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = DescriptionActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = DescriptionActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.descRV)
    RecyclerView descRV;
    @BindView(R.id.txtHeadingTV)
    TextView txtHeadingTV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.imgMenuIV)
    ImageView imgMenuIV;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;
    DescriptionAdapter mDescAdapter;
    List<DescriptionModel.Datum> mDescCategoryList;
    String category_id;
    String name;

    @BindView(R.id.imgSearchIV)
    ImageView imgSearchIV;
    @BindView(R.id.searchRL)
    RelativeLayout searchRL;

    @BindView(R.id.editSearchET)
    EditText editSearchET;
    @BindView(R.id.imgCancelIV)
    ImageView imgCancelIV;
    List mRecentItemsList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);
        setStatusBar(mActivity);
        ButterKnife.bind(this);
        getIntentData();
        txtHeadingTV.setText(name);
        imgBackIV.setVisibility(View.VISIBLE);
        imgMenuIV.setVisibility(View.GONE);
        getDescriptionData();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            if (Objects.requireNonNull(getIntent().getExtras()).get("category_id") != null) {
                category_id = (String) getIntent().getExtras().get("category_id");
                name = (String) getIntent().getExtras().get("name");
            }
        }
    }


    @OnClick({R.id.imgSearchIV, R.id.imgBackIV,R.id.imgCancelIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgSearchIV:
                searchRL.setVisibility(View.VISIBLE);
                txtHeadingTV.setVisibility(View.GONE);
                imgMenuIV.setVisibility(View.GONE);
                imgSearchIV.setVisibility(View.GONE);
                performSearchClick();
                break;
            case R.id.imgBackIV:
               performBackClick();
                break;
            case R.id.imgCancelIV:
                performCancelClick();
                break;
        }
    }

    private void performBackClick() {
        onBackPressed();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    private void performCancelClick() {
        hideKeyBoard(mActivity,getCurrentFocus());
        searchRL.setVisibility(View.GONE);
        txtHeadingTV.setVisibility(View.VISIBLE);
        imgMenuIV.setVisibility(View.GONE);
        imgSearchIV.setVisibility(View.VISIBLE);
        editSearchET.setText("");
        getDescriptionData();
    }

    private void SearchItem() {
        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // filter your list from your input
                filter(s.toString());
                //you can use runnable postDelayed like 500 ms to delay search text
            }
        });

        editSearchET.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //do here your stuff f
                    if (mRecentItemsList.size()==0){
                        showToast(mActivity,"No Result found");

                    }
                    hideKeyBoard(mActivity,getCurrentFocus());
                    return true;
                }
                return false;
            }
        });
    }

    private void getDescriptionData() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetDescriptionApi();
        }

    }

    void filter(String text) {
        mRecentItemsList = new ArrayList();
        for (DescriptionModel.Datum d : mDescCategoryList) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d.getTitle().toLowerCase().contains(text.toLowerCase())) {
                mRecentItemsList.add(d);
            }
        }
        //update recyclerview
        if (mDescCategoryList.size()>0){
            mDescAdapter.updateList(mRecentItemsList);
        }
//        if (mRecentItemsList.size()==0){
//            showToast(mActivity,"No Result found");
//
//        }
    }

    private void performSearchClick() {
SearchItem();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("c_id", category_id);
        mMap.put("search", "");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetDescriptionApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.descriptionRequest(mParam()).enqueue(new Callback<DescriptionModel>() {
            @Override
            public void onResponse(Call<DescriptionModel> call, Response<DescriptionModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                DescriptionModel mModel = response.body();
                mDescCategoryList = new ArrayList<>();
                mDescCategoryList = mModel.getData();
                if (mModel.getStatus() == 1) {
                    setDescAdapter();
                    txtNoDataFountTV.setVisibility(View.GONE);
                } else {
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                    txtNoDataFountTV.setText(mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<DescriptionModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    private void setDescAdapter() {
        mDescAdapter = new DescriptionAdapter(mActivity, mDescCategoryList);
        descRV.setLayoutManager(new LinearLayoutManager(mActivity));
        descRV.setAdapter(mDescAdapter);
        mDescAdapter.notifyDataSetChanged();
    }

}