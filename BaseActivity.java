package com.candaceyoung.app.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.candaceyoung.app.R;
import com.candaceyoung.app.utils.CandaceyoungPreferences;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class BaseActivity extends AppCompatActivity {

    /*
    * Get Class Name
    * */
    String TAG = BaseActivity.this.getClass().getSimpleName();
    /*
    * Initialize Activity
    * */
    Activity mActivity = BaseActivity.this;

    /*
    * Initialize Other Classes Objects...
    * */
    public Dialog progressDialog;

    /*
    * Get Is User Login
    * */
    public Boolean IsUserLogin(){
        return CandaceyoungPreferences.readBoolean(mActivity, CandaceyoungPreferences.ISLOGIN,false);
    }
    /*
    * Get User ID
    * */
    public String getID(){
        return CandaceyoungPreferences.readString(mActivity, CandaceyoungPreferences.ID,"");
    }

    /*
    * Get User Email ID
    * */
    public String getUserEmailID(){
        return CandaceyoungPreferences.readString(mActivity, CandaceyoungPreferences.EMAIL,"");
    }

    /*
    * Get User Name
    * */
    public String getUserName(){
        return CandaceyoungPreferences.readString(mActivity, CandaceyoungPreferences.FIRST_NAME,"");
    }

    /*
    * Get User Profile Picture
    * */
    public String getUserProfilePicture(){
        return CandaceyoungPreferences.readString(mActivity, CandaceyoungPreferences.IMAGE,"");
    }

    /*
    transparent status bar
     */
    public void setStatusBar(Activity mActivity){
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            mActivity.getWindow().getDecorView().setSystemUiVisibility(View.STATUS_BAR_VISIBLE);
//            // edited here
//            mActivity.getWindow().setStatusBarColor(getResources().getColor(R.color.colorBlack));
//        }
}

    /*
     * Finish the activity
     * */
    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionSlideDownExit();
    }

//    /*to switch between fragments*/
//    public void switchFragment(final Fragment fragment, final String Tag, final boolean addToStack, final Bundle bundle) {
//        FragmentManager fragmentManager= getSupportFragmentManager();
//        if (fragment != null) {
//            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentTransaction.replace(R.id.frameL, fragment, Tag);
//            if (addToStack)
//                fragmentTransaction.addToBackStack(Tag);
//            if (bundle != null)
//                fragment.setArguments(bundle);
//            fragmentTransaction.commit();
//            fragmentManager.executePendingTransactions();
//        }
//    }


    /*
     * To Start the New Activity
     * */
    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionSlideUPEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    public void overridePendingTransitionSlideUPEnter() {
        overridePendingTransition(R.anim.bottom_up, 0);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    public void overridePendingTransitionSlideDownExit() {
        overridePendingTransition(0, R.anim.bottom_down);
    }


    public void editTextSelector(final EditText mEditText, RelativeLayout rl, String tag) {
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                rl.setBackgroundResource(R.drawable.bg_et_profile);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 0) {
                        rl.setBackgroundResource(R.drawable.bg_pro_sel_et);
                } else {
                        rl.setBackgroundResource(R.drawable.bg_pro_sel_et);
                }

            }
        });
    }

    public void editTextSignupSelector(final EditText mEditText, RelativeLayout rl, String tag) {
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                rl.setBackgroundResource(R.drawable.bg_et_white);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 0) {
                        rl.setBackgroundResource(R.drawable.bg_et);
                } else {
                        rl.setBackgroundResource(R.drawable.bg_et_white);
                }

            }
        });
    }

    /*
     * Show Progress Dialog
     * */

    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null)
            progressDialog.show();
    }



    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    /*
     * Hide Keyboard
     * */
    public boolean hideKeyBoard(Context context, View view) {
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        return true;
    }

    /*
     * Validate Email Address
     * */
    public boolean isValidEmaillId(String email) {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    /*
     * Share Gmail Intent
     * */
    public void shareIntentGmail(Activity mActivity, String profileUrl) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Deal Breaker:");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "Hey check out friend profile and share your view : " + profileUrl);
        mActivity.startActivity(Intent.createChooser(sharingIntent, "Choose One"));
    }

    /*
     * Check Internet Connections
     * */
    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    /*
     * Toast Message
     * */
    public void showToast(Activity mActivity, String strMessage) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
    }


    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialogFinish(final Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mActivity.finish();
            }
        });
        alertDialog.show();
    }

        /*
         * Date Picker Dialog
         * */
    public void showDatePickerDialog(Activity mActivity, final TextView mTextView) {
        int mYear, mMonth, mDay, mHour, mMinute;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        int intMonth = monthOfYear + 1;
                        mTextView.setText(year + "-" + getFormatedString("" + intMonth) + "-" + getFormatedString("" + dayOfMonth));
                    }
                }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }


    public boolean isValidDateOfBirth(TextView mTextView) {
        boolean isDateValid = false;
        String dob = mTextView.getText().toString().trim();

        long mTimeInLong = getTimeInLong(dob);
        long difference = System.currentTimeMillis() - mTimeInLong;
        long dy = TimeUnit.MILLISECONDS.toDays(difference);

        final long years = dy / 365;

        isDateValid = years >= 18;
        Log.e(TAG, "isValidDateOfBirth: " + isDateValid);
        Log.e(TAG, "isValidDateOfBirth: " + years);

        return isDateValid;
    }


    private long getTimeInLong(String mDate) {
        long startDate = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date date = sdf.parse(mDate);

            startDate = date.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return startDate;
    }


    public String getFormatedString(String strTemp) {
        String strActual = "";

        if (strTemp.length() == 1) {
            strActual = "0" + strTemp;
        } else if (strTemp.length() == 2) {
            strActual = strTemp;
        }

        return strActual;
    }


    /**
     * Turn drawable into byte array.
     *
     * @param drawable data
     * @return byte array
     */
    public static byte[] getFileDataFromDrawable(Context context, Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public String getCurrentDate() {
        Date d = new Date();
        CharSequence mCurrentDate = DateFormat.format("dd/mm/yyyy", d.getTime());
        return mCurrentDate.toString();
    }


}
