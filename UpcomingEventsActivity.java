package com.candaceyoung.app.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.candaceyoung.app.R;
import com.candaceyoung.app.RetrofitApi.ApiClient;
import com.candaceyoung.app.interfaces.ApiInterface;
import com.candaceyoung.app.model.SendPushModel;
import com.candaceyoung.app.utils.CandaceyoungPreferences;
import com.joooonho.SelectableRoundedImageView;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpcomingEventsActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = UpcomingEventsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = UpcomingEventsActivity.this;

    /*
     * Widgets
     * */

    @BindView(R.id.txtHeadingTV)
    TextView txtHeadingTV;

    @BindView(R.id.imgEditIV)
    ImageView imgEditIV;

    @BindView(R.id.imgMenuIV)
    ImageView imgMenuIV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.txtSubmitTV)
    TextView txtSubmitTV;
    @BindView(R.id.imgAddLinkIV)
    SelectableRoundedImageView imgAddLinkIV;
    @BindView(R.id.titleRL)
    RelativeLayout titleRL;
    @BindView(R.id.linkRL)
    RelativeLayout linkRL;
    @BindView(R.id.editTitleET)
    EditText editTitleET;
    @BindView(R.id.editLinkET)
    EditText editLinkET;
    Bitmap selectedImage;
    String mBase64Image = "";
    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upcoming_events);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        txtHeadingTV.setText(R.string.upcoming_events);
        imgEditIV.setVisibility(View.GONE);
        imgMenuIV.setVisibility(View.GONE);
        imgBackIV.setVisibility(View.VISIBLE);
        editTextSelector(editTitleET, titleRL, "");
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", CandaceyoungPreferences.readString(mActivity,CandaceyoungPreferences.ID,null));
        mMap.put("title", editTitleET.getText().toString().trim());
        mMap.put("link", editLinkET.getText().toString().trim());
        mMap.put("eventImage", mBase64Image);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeAddEventApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addEventRequest(mParam()).enqueue(new Callback<SendPushModel>() {
            @Override
            public void onResponse(Call<SendPushModel> call, Response<SendPushModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SendPushModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    Toast.makeText(mActivity, mModel.getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SendPushModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());


            }
        });
    }

    @OnClick({R.id.txtSubmitTV, R.id.imgBackIV,R.id.imgAddLinkIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtSubmitTV:
                performAddEventClick();
                break;
            case R.id.imgBackIV:
                performBackClick();
                break;
            case R.id.imgAddLinkIV:
                performImageClick();
                break;
        }
    }
    /* Camera Gallery functionality
     * */
    private void performImageClick() {
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private void onSelectImageClick() {
        CropImage.startPickImageActivity(mActivity);
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setAspectRatio(70, 70)
                .setMultiTouchEnabled(false)
                .start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(mActivity, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(mActivity, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                try {

                    final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
                    selectedImage = BitmapFactory.decodeStream(imageStream);
                    String path = MediaStore.Images.Media.insertImage(mActivity.getContentResolver(), selectedImage, "IMG_", null);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 10, baos);
                    RequestOptions options = new RequestOptions()
                            .placeholder(R.drawable.ic_upload_image)
                            .error(R.drawable.ic_upload_image)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .skipMemoryCache(true)
                            .dontAnimate()
                            .dontTransform();

                    if (path != null) {
                        Glide.with(mActivity).load(path)
                                .apply(options)
                                .into(imgAddLinkIV);

                    }
                    mBase64Image = encodeTobase64(selectedImage);

                    Log.e(TAG, "**Image Base 64**" + mBase64Image);


                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(mActivity, "Cropping failed: " + result.getError());
            }
        }
    }

    private String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 30, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    private void performBackClick() {
        onBackPressed();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void performAddEventClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showToast(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeAddEventApi();
            }
        }}

    /*
     * Set up validations
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (mBase64Image.equals("")) {
            showAlertDialog(mActivity, getString(R.string.select_image));
            flag = false;
        } else if (editTitleET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.enter_title));
            flag = false;
        } else if (editLinkET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.enter_link));
            flag = false;
        } else if (!isValidUrl(editLinkET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.enter_valid_link));
            flag = false;
        } else if (!editLinkET.getText().toString().trim().contains("https://")) {
            showAlertDialog(mActivity, getString(R.string.enter_valid_link));
            flag = false;
        }
        return flag;
    }

    private boolean isValidUrl(String url) {
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(url.toLowerCase());
        return m.matches();
    }

}

